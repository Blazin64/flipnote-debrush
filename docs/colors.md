# Colors

This document explains how shades of color are assigned to Flipnote brush patterns.

## Palette

### Flipnote Studio

The color palette on the original Flipnote Studio is limited to just 4 colors! That's not the only limitation, either! Only 3 colors are allowed per frame - a background color and two single-color layers.

![Flipnote Studio Palette](images/color/palette-dsi.png)

### Flipnote Studio 3D

Currently, this project does not support 3D Flipnotes produced with Flipnote Studio 3D.

The range of the color palette on Flipnote Studio 3D is greatly improved over the original. Oddly, red and blue have duller shades than they did on the original. Maybe Nintendo was trying to keep the colors from overpowering each other?

The number of colors allowed per frame was also improved. This version of Flipnote Studio has 3 drawing layers instead of two. Each of these layers supports two colors at once, so it is possible to use the entire palette in a single frame!

![Flipnote Studio Palette](images/color/palette-3ds.png)

## Shading Methods

### Scaled + Blurred Shading

This was made by slightly blurring the brush pattern's image and then shrinking it by 50% (or more). Once that was done, a color sample was taken. It is by far the lightest shading method. It may also be the most accurate.

![Scaled Shading Image](images/color/shading-scale-blur.png)

Pixel values: 0, 94, 137, 188, 225, 242, 255

### Percent Shading

This was made by using the ratio of black and white pixels in brush patterns as raw RGB values.

![Percent Shading Image](images/color/shading-percent.png)

Pixel values: 0, 28, 64, 127, 191, 222, 255

### Linear Shading

Lienar shading was the original method of choice for this project, due to the level of contrast between shades. It turned out to be too inaccurate.

This was made by incrementing the raw RGB values 17% steps. It is slightly brighter than percent shading at the start, but darker at the end.

![Linear Shading Image](images/color/shading-linear.png)

Pixel values: 0, 42, 85, 128, 170, 212, 255

### Perception Shading

This was made by incrementing the "value" in the HCL colorspace in 17% steps. It is very slightly darker than linear shading in most cases.

![Scaled Shading Image](images/color/shading-perception.png)

Pixel values: 0, 42, 78, 116, 163, 209, 255
