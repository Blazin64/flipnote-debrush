# Flipnote Debrush AI

### Description

Flipnote Debrush AI is an ESRGAN model trained to replace brush patterns in Flipnote animations with solid colors.

### Why?

Brush patterns in Flipnote animations were often used to simulate additional shades of color, since the Flipnote color palette is extremely limited. The illusion worked quite well on tiny Nintendo DSi screens.

Unfortunately, the illusion fails on larger displays, like TVs or monitors. It is very hard to look at. Worse, it fails to convey the shades of color the artist intended.

By replacing brush patterns with (approximations of) the intended shades of color, Flipnotes can be more comfortably viewed on larger displays. The appearance will, hopefully, be close to what the artist intended.

### Side Benefits

Replacing the brush patterns with solid colors makes Flipnote animations much easier to upscale with AI upscalers. Assuming this project matures enough to achieve good quality, an upscaling model may be added. Time will tell...

### NOTE

Currently, the traning dataset is fairly small at 26 training images. Eventually, significantly more synthetic and real-world images will be added. This should improve consistency significantly.

# Examples

* The following images are typical examples of what the ESRGAN model can currently do. The model needs to be trained on a larger dataset before high quality can be expected consistently.

* The original images are shown on the left side. The "debrushed" images are shown on the right side.

* You may notice that the shades of color are slightly too dark in these examples. It is an easily corrected issue with the training data. Future versions will fix that.

![Example Image 0](docs/images/examples/example0.png)

![Example Image 1](docs/images/examples/example1.png)

![Example Image 2](docs/images/examples/example2.png)

![Example Image 3](docs/images/examples/example3.png)

The images above are from animations made by Mcboo and Retroman. Since these are negligibly small portions of the original animations, this seems to qualify for fair use.

# Performance

An NVIDIA RTX 3070 was able to achive 4.71 FPS when using the Flipnote Debrush AI model on [iNNfer](https://github.com/victorca25/iNNfer). Other ESRGAN implementations, such as the one used by [Cupscale](https://github.com/n00mkrad/cupscale), may perform differently.

# Usage

* Find some Flipnote `.ppm` or `.kwz` animation files.
* Install [flipnote-video](https://github.com/jaames/flipnote-video).
* Convert your Flipnote `.ppm` file to a lossless MKV file.
  * `flipnote-video -i flipnote.ppm -o -c:v libx264 -c:a pcm_s16le -crf 0 flipnote.mkv`
* Install [Cupscale](https://github.com/n00mkrad/cupscale).
* Download [flipnote-debrush-concept-80k.pth](models/flipnote-debrush-concept-80k.pth) into your Cupscale model folder.
* Select the model you downloaded in Cupscale
* Select your MKV file in the "Upscale Videos" tab.
* Click "Upscale and Save"
* Enjoy!

# Training

For this particular project, [traiNNer](https://github.com/victorca25/traiNNer) is used to train the model. See the [Beginner Training Guide](https://upscale.wiki/wiki/Beginner_Training_Guide) on the Upscale Wiki for details on the training process.

A purely synthetic version of the dataset used to train this model can be found in the [dataset](dataset) directory. Make sure to duplicate the "train" folder and call the copy "val". (This is technically bad practice for machine learning, but the dataset is too small at the moment for a separate evaluation dataset to be feasible.)

The file called [train_sr.yml](dataset/train_sr.yml) contains the traiNNer configuration used for training. Use this file as a replacment for the one included with traiNNer.

# Dataset

The dataset that has been published here is entirely synthetic. There are additional real images that were also used for training. Unfortunately, the creators who made many of the source animations cannot be contacted easily. It would be a bad idea to publish very large portions of their work without permission.

Real images will be added to the public dataset when permission is acquired.
